<?php

namespace App\Http\Livewire;

use Livewire\Component;

class VisitorIndex extends Component
{

    public function render()
    {
        return view('livewire.visitor-index');
    }
}
