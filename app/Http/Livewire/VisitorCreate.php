<?php

namespace App\Http\Livewire;

use App\Visitor;
use Livewire\Component;
use Request;
use Illuminate\Support\Facades\Http;

class VisitorCreate extends Component
{
    public $access = [];

    public function assign_acces_prop($prop)
    {
        array_push($this->access ,$prop);
    }

    public function render()
    {
        return view('livewire.visitor-create');
    }

    private function resetInput(){
        $this->access = '';
    }

    public function store ()
    {
        $ip = '134.201.250.155';
        $response = Http::get('https://api.ipgeolocation.io/ipgeo?apiKey=b354dde9ce704047a41152be9a123342&ip='.$ip);
        $data = $response->json();
        $insert =  Visitor::create([
            'city' => $data['country_capital'],
            'country' => $data['country_name'],
            'ip' => $data['ip'],
            'isp' => $data['isp'],
            'access' => 'dummy'
         ]);
         $this->resetInput();
    }
}
