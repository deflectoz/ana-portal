<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>KJPP Amin Nirwan Alfiantori & Rekan Apps Portal</title>
    {{-- Icon --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/cover/">
    <!-- Bootstrap core CSS -->
<link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/cover.css') }}" rel="stylesheet">
  </head>
  <body class="d-flex h-100 text-center text-white">
    
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
  <header class="mb-auto">
    <div>
    
    </div>
  </header>
@livewireStyles
  <main class="px-3">
      <livewire:visitor-index></livewire:visitor-index>
  </main>

  <footer class="mt-auto text-white-50">
    <p>Made With <i class="bi-emoji-sunglasses"></i> And Proudly Made By MF With <i class="bi-headphones"></i>.</p>
  </footer>
</div>
  </body>
  @livewireScripts
  <script src="{{ asset('/js/cover.js') }}"></script>
</html>
