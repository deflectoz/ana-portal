<div>
    <div class="row">
        <div class="col-md-3">
        <form wire:submit.prevent="store">
            <input type="hidden" wire:model="access">
            <button wire:click.prevent="assign_acces_prop('Website')" class="btn-change"><i class="bi-globe"></i> Website</button>
            <button class="btn btn-sm btn-success text-white btn-block"><i class="bi bi-save-fill"></i> Simpan Data</button>
        </form>
    </div>
    <div class="col-md-3">
        <form action="">
            <input type="hidden" wire:model="access">
           <button wire:click.prevent="assign_acces_prop('Nas Storage')" class="btn-change"><i class="bi-cloud"></i> Storage</button>
        </div>
        </form>
        <div class="col-md-3">
        <form action="">
            <input type="hidden" wire:model="access">
            <button wire:click.prevent="assign_acces_prop('Webmail')" class="btn-change"><i class="bi-mailbox"></i> Webmail</button>
        </form>
    </div>
    <div class="col-md-3">
      <form action="">
        <input type="hidden" wire:model="access">
        <button wire:click.prevent="assign_acces_prop('IBPA')" class="btn-change"><i class="bi-terminal"></i> IBPA Robot</button>
      </form>
    </div>
    </div>
</div>
